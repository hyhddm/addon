# -*- coding: utf-8 -*-
#############################################################################
#
#
#############################################################################

{
    'name': '总价销售折扣',
    'version': '14.0.1.0.0',
    'category': '销售管理',
    'live_test_url': 'https://cdn.openerp.hk',
    'summary': "在审批后的销售总价折扣",
    'company': 'OpenERP.HK',
    'website': 'https://cdn.openerp.hk',
    'description': """

销售总价折扣
=======================

""",
    'depends': ['sale',
                'account', 'delivery'
                ],
    'data': [
        'views/sale_view.xml',
        'views/account_invoice_view.xml',
        'views/invoice_report.xml',
        'views/sale_order_report.xml',
        'views/res_config_view.xml',

    ],
    'images': ['static/description/banner.png'],
    'license': 'AGPL-3',
    'application': True,
    'installable': True,
    'auto_install': False,
}
