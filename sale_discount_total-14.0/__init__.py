# -*- coding: utf-8 -*-
#############################################################################
#
#    OpenERP.HK Team
#    请遵循AGPL v3 协议
#    (AGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
#############################################################################
from . import models
from . import reports

